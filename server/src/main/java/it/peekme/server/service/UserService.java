package it.peekme.server.service;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import it.peekme.server.ErrorCodes;
import it.peekme.server.IServer;
import it.peekme.server.model.User;
import it.peekme.server.async.AsyncCallback;
import it.peekme.server.async.AsyncExecutor;
import it.peekme.server.async.AsyncRunnable;
import it.peekme.server.exception.ServerException;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by andre on 02/08/2016.
 */
public class UserService extends AbstractService {

    private String mSessionToken;
    private User mCurrentUser;

    public UserService(Context context, IServer server) {
        super(context, server);
    }

    public User register(User user) throws ServerException {
        try {
            Response response = doRequest("users", new Request.Builder()
                    .post(RequestBody.create(JSON, user.toJson().toString())));
            String body = response.body().string();
            Log.v("Body", body);
            JSONObject jsonResponse = new JSONObject(body);
            if (jsonResponse.optJSONObject("error") != null) {
                JSONObject errorObj = jsonResponse.getJSONObject("error");
                throw new ServerException(errorObj.getInt("code"), errorObj.optString("message"));
            }
            return new User(jsonResponse.getJSONObject("user"));
        } catch (JSONException | IOException e) {
            throw new ServerException(ErrorCodes.ERROR_INTERNAL, e.getMessage());
        }
    }

    public void registerAsync(final User user, AsyncCallback<User> callback) {
        AsyncExecutor.runInBackground(new AsyncRunnable<User>() {

            @Override
            public User run() throws ServerException {
                return register(user);
            }

        }, callback);
    }

    public User login(User user) throws ServerException {
        try {
            JSONObject request = new JSONObject();
            request.put("nickname", user.getNickname());
            request.put("password", user.getPassword());
            Response response = doRequest("users/login", new Request.Builder()
                    .post(RequestBody.create(JSON, request.toString())));
            String body = response.body().string();
            Log.v("Body", body);
            JSONObject jsonResponse = new JSONObject(body);
            if (jsonResponse.optJSONObject("error") != null) {
                JSONObject errorObj = jsonResponse.getJSONObject("error");
                throw new ServerException(errorObj.getInt("code"), errorObj.optString("message"));
            }
            mSessionToken = jsonResponse.getString("token");
            mCurrentUser = user;
            // TODO find the best way to persist data on disk (SharedPreferences or android keystore ?).
            return user;
        } catch (JSONException | IOException e) {
            throw new ServerException(ErrorCodes.ERROR_INTERNAL, e.getMessage());
        }
    }

    public void loginAsync(final User user, AsyncCallback<User> callback) {
        AsyncExecutor.runInBackground(new AsyncRunnable<User>() {

            @Override
            public User run() throws ServerException {
                return login(user);
            }

        }, callback);
    }

    public User getCurrentUser() {
        return mCurrentUser;
    }

    public boolean isUserLogged() {
        return mCurrentUser != null && mSessionToken != null;
    }
}