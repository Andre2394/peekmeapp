package it.peekme.server.service;

import android.content.Context;

import java.io.IOException;

import it.peekme.server.IServer;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 02/08/2016.
 */
public abstract class AbstractService {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private Context mContext;
    private IServer mServer;

    public AbstractService(Context context, IServer server) {
        mContext = context;
        mServer = server;
    }

    protected Context getContext() {
        return mContext;
    }

    protected Response doRequest(String uri, Request.Builder builder) throws IOException {
        return mServer.doRequest(uri, builder);
    }
}