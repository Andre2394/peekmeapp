package it.peekme.server.async;

import android.support.v4.content.ParallelExecutorCompat;

import java.util.concurrent.Executor;

/**
 * Created by andre on 02/08/2016.
 */
public class AsyncExecutor {

    private static AsyncExecutor mExecutor;

    public static void initialize() {
        mExecutor = new AsyncExecutor();
    }

    private Executor mBaseExecutor;

    public AsyncExecutor() {
        mBaseExecutor = ParallelExecutorCompat.getParallelExecutor();
    }

    public static <T> void runInBackground(AsyncRunnable<T> runnable, AsyncCallback<T> callback) {
        Task<T> task = new Task<>(runnable, callback);
        task.executeOnExecutor(mExecutor.mBaseExecutor);
    }
}