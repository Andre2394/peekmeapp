package it.peekme.server.async;

import it.peekme.server.exception.ServerException;

/**
 * Created by andre on 02/08/2016.
 */
public interface AsyncCallback<T> {

    void onTaskFinished(T item, ServerException e);
}
