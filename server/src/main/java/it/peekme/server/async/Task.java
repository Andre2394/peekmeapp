package it.peekme.server.async;

import android.os.AsyncTask;

import it.peekme.server.exception.ServerException;

/**
 * Created by andre on 02/08/2016.
 */
public class Task<T> extends AsyncTask<Void, Void, T> {

    private AsyncRunnable<T> mRunnable;
    private AsyncCallback<T> mCallback;
    private ServerException mException;

    public Task(AsyncRunnable<T> runnable, AsyncCallback<T> callback) {
        mRunnable = runnable;
        mCallback = callback;
    }

    @Override
    protected T doInBackground(Void... params) {
        try {
            return mRunnable.run();
        } catch (ServerException e) {
            mException = e;
        }
        return null;
    }

    @Override
    public void onPostExecute(T result) {
        if (mCallback != null) {
            mCallback.onTaskFinished(result, mException);
        }
    }
}