package it.peekme.server;

import android.content.Context;

import java.io.IOException;

import it.peekme.server.async.AsyncExecutor;
import it.peekme.server.service.UserService;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 02/08/2016.
 */
public class Server implements IServer {

    private static Server mInstance;

    public static void initialize(Context context, String address, String version) {
        mInstance = new Server(context, address, version);
        AsyncExecutor.initialize();
    }

    private Context mContext;
    private String mBaseUrl;
    private OkHttpClient mClient;
    private UserService mUserService;

    private Server(Context context, String address, String version) {
        mContext = context;
        mBaseUrl = address + "/" + version + "/";
        mClient = new OkHttpClient();
        mUserService = new UserService(context, this);
    }

    @Override
    public Response doRequest(String uri, Request.Builder builder) throws IOException {
        Request request = builder.url(mBaseUrl + uri).build();
        return mClient.newCall(request).execute();
    }

    public static UserService getUserService() {
        return mInstance.mUserService;
    }
}