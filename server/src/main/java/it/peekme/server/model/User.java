package it.peekme.server.model;

import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by andre on 02/08/2016.
 */
public class User extends AbstractModel {

    private long mId;
    private String mNickname;
    private String mName;
    private String mSurname;
    private String mPassword;
    private String mEmail;
    private String mSex;
    private int mAge;

    public User() {

    }

    public User(JSONObject jsonObject) throws JSONException {
        mId = jsonObject.getLong("id");
        mNickname = jsonObject.getString("nickname");
        mName = jsonObject.getString("name");
        mSurname = jsonObject.getString("surname");
        mEmail = jsonObject.getString("email");
        mSex = jsonObject.getString("sex");
        mAge = jsonObject.getInt("age");
    }

    public void setNickname(String nickname) {
        mNickname = nickname;
    }

    public String getNickname() {
        return mNickname;
    }

    public void setName(String name) {
        mName = name;
    }

    public void setSurname(String surname) {
        mSurname = surname;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setSex(String sex) {
        mSex = sex;
    }

    public void setAge(int age) {
        mAge = age;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", mId);
        jsonObject.put("nickname", mNickname);
        jsonObject.put("name", mName);
        jsonObject.put("surname", mSurname);
        if (!TextUtils.isEmpty(mPassword)) {
            jsonObject.put("password", mPassword);
        }
        jsonObject.put("email", mEmail);
        jsonObject.put("sex", mSex);
        jsonObject.put("age", mAge);
        return jsonObject;
    }
}