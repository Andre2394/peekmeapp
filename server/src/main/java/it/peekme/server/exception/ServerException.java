package it.peekme.server.exception;

/**
 * Created by andre on 02/08/2016.
 */
public class ServerException extends Exception {

    private int mCode;

    public ServerException(int code, String message) {
        super(message);
        mCode = code;
    }

    public int getCode() {
        return mCode;
    }
}