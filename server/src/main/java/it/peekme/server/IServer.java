package it.peekme.server;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by andre on 02/08/2016.
 */
public interface IServer {

    Response doRequest(String uri, Request.Builder builder) throws IOException;
}