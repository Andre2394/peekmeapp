package it.peekme.app.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import it.peekme.app.R;
import it.peekme.server.Server;
import it.peekme.server.async.AsyncCallback;
import it.peekme.server.exception.ServerException;
import it.peekme.server.model.User;

/**
 * Created by andre on 02/08/2016.
 */
public class RegisterActivity extends AppCompatActivity implements AsyncCallback<User> {

    Button okButton;
    EditText email;
    EditText username;
    EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Glide.with(this)
                .load(getIntent().getData())
                .placeholder(R.drawable.panorama)
                .centerCrop()
                .into(((ImageView) findViewById(R.id.backgroundRegisterImage)));
        okButton = (Button) findViewById(R.id.confirmRegistration);
        email = (EditText) findViewById(R.id.emailRegister);
        username = (EditText) findViewById(R.id.usernameRegister);
        password = (EditText) findViewById(R.id.passwordIdRegister);
        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                User user = new User();
                user.setNickname(username.getText().toString());
                user.setEmail(email.getText().toString());
                user.setPassword(password.getText().toString());
                Server.getUserService().registerAsync(user, RegisterActivity.this);
            }

        });
    }

    @Override
    public void onTaskFinished(User item, ServerException e) {
        if (e != null) {
            // error
            Toast.makeText(this, "ERROR: code " + e.getCode() + " - " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "User " + item.getNickname() + " successfully registered, please check the email box for the activation link", Toast.LENGTH_LONG).show();
        }
    }
}