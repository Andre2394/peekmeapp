package it.peekme.app.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import it.peekme.app.R;

/**
 * Created by andre on 02/08/2016.
 */
public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Glide.with(this)
                .load(getIntent().getData())
                .placeholder(R.drawable.panorama)
                .centerCrop()
                .into(((ImageView) findViewById(R.id.backgroundLoginImage)));
    }
}
