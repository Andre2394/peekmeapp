package it.peekme.app;

import it.peekme.server.Server;

/**
 * Created by andre on 02/08/2016.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Server.initialize(this, "http://peekme.altervista.org", "v1");
    }
}