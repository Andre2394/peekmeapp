### PeekMe ###

**User**

1. Registrazione: 
* Method: POST
* Link: .../users
* Content-type: application/json
* Body: nickname, email, password, [name, surname, age, sex]
* Response: json object
* Note: una mail viene automaticamente spedita all'indirizzo di registrazione.

2. Attivazione account:
* Method: GET
* Link: .../users/activate
* Parameters: email, token
* Response: json object
* Note: nel caso in cui la mail e il token corrispondono ad un utente non attivo, questo viene attivato.

3. Login:
* Method: POST
* Link: .../users/login
* Content-type: application/json
* Body: nickname, password
* Response: json object with "token".
* Note: il token identifica una sessione aperta, va aggiunto agli headers come X-Authorization nelle richieste successive